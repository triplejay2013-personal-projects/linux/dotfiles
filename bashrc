# Functions
source ~/.shell/functions.sh

# Allow local customizations in the ~/.shell_local_before file
if [ -f ~/.shell/local_before ]; then
    source ~/.shell/local_before
fi

# Allow local customizations in the ~/.bashrc_local_before file
if [ -f ~/.bashrc_local_before ]; then
    source ~/.bashrc_local_before
fi

# Bootstrap
source ~/.shell/bootstrap.sh

# Completion
source ~/.bash/complete.bash

# Settings
source ~/.bash/settings.bash

# External settings
source ~/.shell/external.sh

# Aliases
source ~/.shell/aliases.sh

# Custom prompt
source ~/.bash/prompt.bash

# Plugins
source ~/.bash/plugins.bash

# Enviornment
source ~/.bash/environment.bash

# Allow local customizations in the ~/.shell_local_after file
if [ -f ~/.shell/local_after ]; then
    source ~/.shell/local_after
fi

# Allow local customizations in the ~/.bashrc_local_after file
if [ -f ~/.bashrc_local_after ]; then
    source ~/.bashrc_local_after
fi

# Will only be set after dotfiles setup has finished, can be used as flag to check installation
export DOTFILES_INSTALLED=true
