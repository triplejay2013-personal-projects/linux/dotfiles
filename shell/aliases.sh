# ======================
# Common actions/aliases
# ======================

alias aliases="alias -p"

# Utility
alias tmux-launch='tmux -2'
alias tmux-resize='tmux detach -a'
alias reload='clear_loaded_files && source ${HOME}/.bash_profile'
alias Git='cd ~/Git'
alias black='python3 -m black'
alias inspect-pop='dirs -c'
alias pipdeptree='python3 -m pipdeptree'
alias rsync='rsync --progress --partial'  # Show progress and keep partial files on fail

# Use colors in coreutils utilities output
alias ls='ls --color=auto'
alias grep='grep --color'

# ls aliases
alias l='ls'
alias lf='ls -f'            # Disable ANSI colors (useful for outputting to file)
alias ls='ls -h --color --group-directories-first'
alias lx='ls -lXB'          # Sort by extension
alias lk='ls -lSr'          # Sort by size biggest last
alias lt='ls -ltr'          # Sort by date, most recent last
alias lc='ls -ltcr'         # Sort by/show change time, most recent last
alias lu='ls -ltur'         # Sort by/show access time, most recent last
alias ll='ls -lv --group-directories-first'
alias lm='ll | less'
alias lr='ll -R'            # Recursive ls
alias la='ls -A'            # Show hidden files
alias tree='tree -Csuh'     # Alt. to recursive ls

# Aliases to protect against overwriting
alias cp='cp -i'
alias mv='mv -i'

# Common aliases
alias mkdir='mkdir -p'
alias h='history'
alias j='jobs'
alias which='type -a'
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'
alias du='du -kh'
alias df='df -kTh'

# dirs
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

# cd to git root directory
alias cdgr='cd "$(git root)"'

# Mirror a website
alias mirrorsite='wget -m -k -K -E -e robots=off'

# Mirror stdout to stderr, useful for seeing data going through a pipe
alias peek='tee >(cat 1>&2)'

# ---------------
# Tailoring Less
# ---------------
alias more='less'
export LESS='-i -N -w  -z-4 -g -e -M -X -F -R -P%t?f%f :stdin .?pb%pb\%:?lbLine %lb:?bbByte %bb:-...'

# LESS man page colors (makes Man pages more readable).
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#-------------------------------------------------------------
# Spelling typos - highly personnal and keyboard-dependent :-)
#-------------------------------------------------------------
alias moer='more'
alias kk='ll'

# ---------
# Terraform
# ---------
alias terraform="TF_LOG=DEBUG TF_LOG_PATH=$HOME/.terraform-logs/terraform.log terraform"

# -----------
# Bash Prompt
# -----------
alias git_extras_disable="clear_loaded_files && export GIT_EXTRAS_ENABLED=0 && source ${HOME}/.bash_profile"
alias git_extras_enable="clear_loaded_files && export GIT_EXTRAS_ENABLED=1 && source ${HOME}/.bash_profile"

#AWSume alias to source the AWSume script
alias awsume="source awsume"
# NOTE: This is a way to use python utilties in bash :thinking face:
