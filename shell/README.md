# About

Common shell configurations

# Structure

* `aliases`: Used in lieu of `.bash_aliases`
* `bootstrap.sh`: Add dotfiles bin to PATH
* `dircolors.extra`: Custom dir colors
* `external.sh`: External tool configuration (pip, python, etc.)
* `functions.sh`: Common functions
* `plugins/`: shell plugins
* `local_*`: Installed dirs/files from dotfiles-local
