# ================
# Common functions
# ================

MY_BASH_COLORS=$HOME/.bash/colors.bash
if [ -f "$MY_BASH_COLORS" ]; then
    # This script is used during install, and colors isn't crtical for that. We
    # Case this out anyways, to avoid annoying error messages :) This should exist
    # after install, though this file should be used.

    # Currently only used for tmux, which isn't critical for the install script
    source $MY_BASH_COLORS
fi
unset MY_BASH_COLORS

# ----
# PATH
# ----
path_remove() {
    PATH=$(echo -n "$PATH" | awk -v RS=: -v ORS=: "\$0 != \"$1\"" | sed 's/:$//')
}

path_append() {
    path_remove "$1"
    PATH="${PATH:+"$PATH:"}$1"
}

path_prepend() {
    path_remove "$1"
    PATH="$1${PATH:+":$PATH"}"
}

# Symlink to current location
here() {
    local loc
    if [ "$#" -eq 1 ]; then
        loc=$(realpath "$1")
    else
        loc=$(realpath ".")
    fi
    ln -sfn "${loc}" "$HOME/.shell.here"
    where
}

there="$HOME/.shell.here"

# Navigate to previously created symlink
there() {
    cd "$(readlink "${there}")"
}

where() {
    echo "here -> $(readlink $HOME/.shell.here)"
}

# Update dotfiles
update_dotfiles() {
    (
        cd ~/.dotfiles && git pull --ff-only && make update
    )
}

# Use pip without requiring virtualenv
syspip() {
    PIP_REQUIRE_VIRTUALENV=false python -m pip "$@"
}

syspip3(){
    PIP_REQUIRE_VIRTUALENV=false python3 -m pip "$@"
}

# Create a directory and cd into it
mcd() {
    mkdir "${1}" && cd "${1}"
}

# Jump to directory containing file
jump() {
    cd "$(dirname ${1})"
}

# cd replacement for screen to track csd (like tmux)
scr_cd() {
    builtin cd $1
    screen -X chdir "$PWD"
}

if [[ -n $STY ]]; then
    alias cd=scr_cd
fi

# Execute a command in a specific directory
xin() {
    (
        cd "${1}" && shift && "${@}"
    )
}

nonascii() {
    rg
    # TODO convert to rg
    LC_ALL=C grep -n '[^[:print:][:space:]]' "${1}"
}

# ----------------------------------------------------------------------------
# Check to see if a file is loaded in current running env. Otherwise load file
# ----------------------------------------------------------------------------
check_and_source() {
    # Examples:
    #   check_and_source $HOME/.bash/colors.bash COLORS_LOADED
    #   check_and_source $HOME/.bash-local/prompt.bash LOCAL_PROMPT_LOADED true

    if [ -z $1 ]; then
        echo "Usage: check_and_source <FILE:str> <FLAG:str> <FAIL_OK:optional>"
        echo "\tFILE is the file to validate/load."
        echo "\tFLAG is a custom flag set in a bash script to indicate the script has already run in current env."
        echo "\tFAIL_OK will skip failure if the file is not found"
        exit 1
    fi
    local MY_FILE=$1
    local FILE_FLAG=${2:-}
    local FAIL_OK=${3:-false}
    if [ -f $MY_FILE ]; then
        # NOTE 'clear_loaded_files' function. Follow the proper syntax for file naming
        if [ -z ${!FILE_FLAG} ]; then
            # Avoid sourcing the same file repeatedly
            source $MY_FILE
        fi
    else
        if [ $FAIL_OK = false ]; then
            echo "Could not lcoate '$MY_FILE'"
            if [ -z "$PS1" ]; then
                sleep 5  # If this is an interactive session, give user time to see error message
            fi
            exit 1
        fi
    fi
}

# ------------------------------------------------------------------------------------------------------
# Clears files that define a '__<FILE>_LOADED' flag, allowing them to be resourced in current context
# ------------------------------------------------------------------------------------------------------
clear_loaded_files() {
    # https://stackoverflow.com/a/32931403
    mapfile -t my_vars < <( env | rg "__.*_LOADED" | awk 'BEGIN{FS="="}{print $1}' | tr '\n' ' ' )
    for v in ${my_vars[@]}; do
        unset ${!v}
    done
}

#-------------------------
# Git navigation/overrides
#-------------------------
cd () {
    # Depends on .gitconfig `git root-full` alias
    # REF: https://stackoverflow.com/a/24039161

    # Capture arguments/flags to builtin cd
    local -a args
    while getopts LP opt "$@"; do
        case $opt in
            -*) args+=( "$opt" ) ;;
        esac
    done
    shift $((OPTIND-1))
    if [[ -z $1 ]]; then
        # Navigate to git root if in git dir, if at root, then navigate to HOME
        if git rev-parse --git-dir > /dev/null 2>&1 && ! [[ "$PWD" == "$(git root-full)" ]]; then
            args+=( "$(git root-full)" )
        else
            args+=( "$HOME" )
        fi
    else
        args+=( "$1" )
    fi
    builtin cd "${args[@]}"
}

#-------------------------------------------------------------
# File & strings related functions:
#-------------------------------------------------------------

# Find a file with a pattern in name:
function ff() { find . -type f -iname '*'"$*"'*' -ls ; }

# Find a file with pattern $1 in name and Execute $2 on it:
function fe() { find . -type f -iname '*'"${1:-}"'*' -exec ${2:-file} {} \;  ; }

#  Find a pattern in a set of files and highlight them:
#+ (needs a recent version of egrep).
function fstr() {
    OPTIND=1
    local mycase=""
    local usage="fstr: find string in files.  Usage: fstr [-i] \"pattern\" [\"filename pattern\"] "
    while getopts :it opt
    do
        case "$opt" in
           i) mycase="-i " ;;
           *) echo "$usage"; return ;;
        esac
    done
    shift $(( $OPTIND - 1 ))
    if [ "$#" -lt 1 ]; then
        echo "$usage"
        return;
    fi
    find . -type f -name "${2:-*}" -print0 | xargs -0 egrep --color=always -sn ${case} "$1" 2>&- | more
}

function swap() {
# Swap 2 filenames around, if they exist.
    local TMPFILE=tmp.$$

    [ $# -ne 2 ] && echo "swap: 2 arguments needed" && return 1
    [ ! -e $1 ] && echo "swap: $1 does not exist" && return 1
    [ ! -e $2 ] && echo "swap: $2 does not exist" && return 1

    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}

function extract() {
    # Handy Extract Program
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

# Creates an archive (*.tar.gz) from given directory.
function maketar() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }

# Create a ZIP archive of a file or folder.
function makezip() { zip -r "${1%%/}.zip" "$1" ; }

# Make your directories and files access rights sane.
function sanitize() { chmod -R u=rwX,g=rX,o= "$@" ;}

#-------------------------------------------------------------
# Process/system related functions:
#-------------------------------------------------------------

function my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,bsdtime,command ; }
function pp() { my_ps f | awk '!/awk/ && $0~var' var=${1:-".*"} ; }

function killps() {
# kill by process name
    local pid pname sig="-TERM"   # default signal
    if [ "$#" -lt 1 ] || [ "$#" -gt 2 ]; then
        echo "Usage: killps [-SIGNAL] pattern"
        return;
    fi
    if [ $# = 2 ]; then sig=$1 ; fi
    for pid in $(my_ps| awk '!/awk/ && $0~pat { print $1 }' pat=${!#} )
    do
        pname=$(my_ps | awk '$1~var { print $5 }' var=$pid )
        if ask "Kill process $pid <$pname> with signal $sig?"
            then kill $sig $pid
        fi
    done
}

function mydf() {
# Pretty-print of 'df' output.
# Inspired by 'dfc' utility.
    for fs ; do

        if [ ! -d $fs ]
        then
          echo -e $fs" :No such file or directory" ; continue
        fi

        local info=( $(command df -P $fs | awk 'END{ print $2,$3,$5 }') )
        local free=( $(command df -Pkh $fs | awk 'END{ print $4 }') )
        local nbstars=$(( 20 * ${info[1]} / ${info[0]} ))
        local out="["
        for ((j=0;j<20;j++)); do
            if [ ${j} -lt ${nbstars} ]; then
               out=$out"*"
            else
               out=$out"-"
            fi
        done
        out=${info[2]}" "$out"] ("$free" free on "$fs")"
        echo -e $out
    done
}

function my_ip() {
# Get IP adress on ethernet.
    MY_IP=$(/sbin/ifconfig eth0 | awk '/inet/ { print $2 } ' |
      sed -e s/addr://)
    echo ${MY_IP:-"Not connected"}
}

function ii() {
# Get current host related info.
    echo -e "\nYou are logged on ${BRed}$HOST"
    echo -e "\n${BRed}Additionnal information:$NC " ; uname -a
    echo -e "\n${BRed}Users logged on:$NC " ; w -hs |
             cut -d " " -f1 | sort | uniq
    echo -e "\n${BRed}Current date :$NC " ; date
    echo -e "\n${BRed}Machine stats :$NC " ; uptime
    echo -e "\n${BRed}Memory stats :$NC " ; free
    echo -e "\n${BRed}Diskspace :$NC " ; mydf / $HOME
    echo -e "\n${BRed}Local IP Address :$NC" ; my_ip
    echo -e "\n${BRed}Open connections :$NC "; netstat -pan --inet;
    echo
}

#-------------------------------------------------------------
# Misc utilities:
#-------------------------------------------------------------

function repeat() {
# Repeat n times command.
    local i max
    max=$1; shift;
    for ((i=1; i <= max ; i++)); do  # --> C-like syntax
        eval "$@";
    done
}


function ask() {
# See 'killps' for example of use.
    echo -n "$@" '[y/n] ' ; read ans
    case "$ans" in
        y*|Y*) return 0 ;;
        *) return 1 ;;
    esac
}

function corename() {
# Get name of app that created a corefile.
    for file ; do
        echo -n $file : ; gdb --core=$file --batch | head -1
    done
}


function swap-clean() {
    for file in $SWAP/*; do
        rm $file
    done
}

function swap-show() {
    for file in $SWAP/*; do
        echo $file
    done
}

# Inspired from:
# - https://unix.stackexchange.com/a/309662
# - https://superuser.com/a/410197
function tmux-find-swp() {
    (
        if [[ -z $1 ]]; then
            echo "Must provide filename to search"
            return 1
        fi
        # Construct expected path/name for swp file
        ORIG=$1; shift
        FNAME=$(echo $ORIG | sed -r 's/\//%/g')
        FNAME="$HOME/.vim/swap/$FNAME.swp"
        if ! [[ -f $FNAME ]]; then
            echo "No swap file found for '$ORIG' matching expected '$FNAME'"
            return 1
        fi
        SWP_PID=$(lsof -t $FNAME)  # Get
        TTY_PID=$(ps -o tty= -p $SWP_PID)
        CUR_SESS=$(tmux display-message -p '#S')  # Get name of running tmux session
        TMUX_SEARCH='#{session_name}:#{window_index}.#{pane_index} #{pane_tty}'
        TMUX_PANES=$(tmux list-panes -a -F "$TMUX_SEARCH" | rg "$TTY_PID$" | rg "$CUR_SESS" | awk '{print $1}')
        if [[ -z $TMUX_PANES ]]; then
            echo "Couldn't find a pane in $CUR_SESS to switch to"
            return 1
        fi
        echo -e "${YEL}Switching to session:pane '${BLE}$TMUX_PANES${YEL}' representing file '${MAG}$ORIG${NC}'${NC}"
        tmux switch -t "$TMUX_PANES"
    )
}

nocolor() {
    if [[ -z $1 ]]; then
        echo "Usage: nocolor <FILE>"
        return 1
    fi
    if [[ -f $1 ]]; then
        sed 's/\x1B\[[0-9;]\{1,\}[A-Za-z]//g' $1
    else
        echo "FILE $1 does not exist"
        return 1
    fi
}

scratchpad() {
    ftype=${1:-txt}
    # Open up tmpfile and delete when done
    tfile=$(mktemp /tmp/scratch.XXXXXXXX.$ftype)
    vim $tfile  # Do stuff in my tmpfile
    rm -f "$tfile"  # TODO trap to make sure file is deleted on error?
}

# Encrypt / Decrypt when entering an enc file
# Utilizes custom mysimplesecrets script
vim-secrets() {
    if [[ -z $1 ]]; then
        echo "Usage: vim-secrets <FILE>"
        return 1
    fi
    MY_ENC_FILE=$1
    MY_DEC_FILE=$(mysimplesecrets decrypt-file --out --silent $MY_ENC_FILE)
    if [[ $? != 0 ]]; then
        echo "File may not be encrypted. Try using 'mysimplesecrets' & 'vim' manually"
        return 1
    fi
    vim $MY_DEC_FILE
    mysimplesecrets encrypt-file $MY_DEC_FILE
}

## TMUX SSH SESSIONS
# HOW TO USE:
#    Place this file inside /usr/local/bin
#    Add verbatim the comment below to /etc/ssh/sshd_config
#    ForceCommand /usr/local/bin/tmux_ssh_session
#      OR
#    ForceCommand tmux_ssh
#           with dotfiles setups
request_tmux() {
  if [[ "$SSH_CONNECTION" != "" && "$MY_SSH_CONNECTION" != "yes" ]]; then
    # TODO list sessions ([1]: [2]:) use number selection to select session
    while true; do
      read -s -n 1 -p "Do you want to attach to a tmux session? [y/n/q]" yn
      case $yn in
        [Yy]* )
          . ~/.bashrc # Source config info found here
          MY_SSH_CONNECTION="yes" tmux attach-session -t cp || MY_SSH_CONNECTION="yes" tmux new -s cp;
          echo -ne "\n"
          break;;
        [Nn]* ) echo -ne "\n"; /bin/bash; break;;
        [Qq]* ) echo -ne "\n"; exit; break;;
        * ) echo "Please answer [y/n]";;
      esac
    done
  fi
}

tmux_ssh() {
  if [[ -n $SSH_ORIGINAL_COMMAND ]]; then
      echo "Running SSH command $SSH_ORIGINAL_COMMAND"
      # If command provided, run it
      exec /bin/bash -c "$SSH_ORIGINAL_COMMAND"
  else
      echo "Requesting tmux session"
      # no command, so request tmux
      request_tmux
  fi
}

# Spin up markdown server (markserv) and allow to be accessed via this machines local IP
markdown() {
    if [[ $PWD = $HOME ]] || ! [[ -d .git ]]; then
        echo "This is intended to be used in git repositories (AND NOT HOME)"
        return 1
    fi
    # REF: https://www.npmjs.com/package/markserv
    SERVING_PORT=8642
    # The IP address assigned via dhcp to this machine
    LOCAL_IP=`sudo ifconfig | rg 'inet (addr:)?([0-9]*\.){3}[0-9]*' | rg 192 | awk '{print $2}'`
    echo "Open in browser: http://$LOCAL_IP:$SERVING_PORT"
    sudo markserv -p $SERVING_PORT -a 0.0.0.0
}


# remove ansi codes from a file
ansi_strip(){
    local FILE=$1
    local OUT=${2:-out.txt}
    if [[ -z $FILE ]]; then
        echo "Usage: ansi_strip <FILE> <OUT:default=out.txt>"
        return 1
    fi
    sed -r "s/\x1B\[(([0-9]+)(;[0-9]+)*)?[m,K,H,f,J]//g" $FILE > $OUT
}

# Run my custom module scripts locally
tf_module_script() {
    if [[ -z $1 ]]; then
        echo "Usage: tf_module_script <script_file_name>"
        return 1
    fi
    curl --silent --fail https://gitlab.com/triplejay2013-personal-projects/projects/terraform_modules/-/raw/main/scripts/$1 &> /dev/null
    if [[ ! $? -eq 0 ]]; then
        echo "$1 does not exist on remote server"
        return 1
    fi
    curl --silent https://gitlab.com/triplejay2013-personal-projects/projects/terraform_modules/-/raw/main/scripts/$1 | bash
}
