# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix M-a
bind-key M-a send-prefix

# split panes using | and -
bind | split-window -h
bind - split-window -v
unbind '"'
unbind %

# reload config file
bind r source-file ~/.tmux.conf; display-message "Reloaded config"

# switch panes using Vim-like nav without prefix
# Also bind arrow keys, for ease-of-use
bind -n M-h select-pane -L
bind -n M-l select-pane -R
bind -n M-k select-pane -U
bind -n M-j select-pane -D
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# Enable mouse mode (tmux 2.1 and above)
setw -g mouse on # Need?

# use vi
setw -g mode-keys vi
# set v to selection as in Vim
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'r' send -X rectangle-toggle

# Copy as in vim
bind-key -T copy-mode-vi 'y' send -X copy-selection-and-cancel
bind-key -T copy-mode-vi 'c' send -X copy-pipe-and-cancel 'xclip -selection clipboard'

# Allow quick escape for vim
set-option -sg escape-time 30  # Default is 500msec y!?

# Default terminal colors
set -g default-terminal "screen-256color"

# Kill session (similar-ish to vim)
bind-key q kill-session

# Sidebar (no focus!!) for tmux
set -g @plugin 'tmux-plugins/tmux-sidebar'

# Save sessions automatically (on reboot e.g.)
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @continuum-save-interval '5'
set -g @continuum-restore 'on'
set -g @resurrect-strategy-vim 'session'

# Default to bash (this should source .bashrc)
set-option -g default-shell "/bin/bash"

# Clear current panes history
bind -n C-x send-keys -R \; clear-history \; send-keys "Enter"

# Enable names for panes
set -g pane-border-status top
# Short command name
# set -g pane-border-format "#P: #{pane_current_command}"
# Command name and args
set -g pane-border-format "#P: #(ps --no-headers -t #{pane_tty} -o args)"

# Toggle mouse on
bind-key M \
  set-option -g mouse on \;\
  display-message 'Mouse: ON'
# Toggle mouse off
bind-key m \
  set-option -g mouse off \;\
  display-message 'Mouse: OFF'

# Increase history limit
set-option -g history-limit 200000
set -g @plugin 'tmux-plugins/tmux-logging'

# vim-gitgutter/vim-tmux-clipboard recommend
set -g focus-events on

# don't rename windows automatically
set-option -g allow-rename off

##########
# DESIGN #
##########
# https://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/
# colors: https://raw.githubusercontent.com/tedsluis/tmux.conf/master/colors/colors.jpg
# I mean....'colours'

# modes
setw -g clock-mode-colour colour5
setw -g mode-style 'fg=colour1 bg=colour37 bold'

# panes
set -g pane-border-style 'fg=colour37 bg=colour0'
set -g pane-active-border-style 'bg=colour0 fg=colour9'

# statusbar
set -g status-position bottom
set -g status-justify left
set -g status-style 'bg=colour37 fg=colour137 dim'
set -g status-left ''
set -g status-right '#[fg=colour233,bg=colour12] %d/%m #[fg=colour233,bg=colour7] %H:%M:%S '

# Current window status bar formats/styles
setw -g window-status-current-style 'fg=colour6 bg=colour107 bold'
setw -g window-status-current-format ' #I#[fg=colour234]:#[fg=colour19]#W#[fg=colour21]#F '

# General window status bar formats/styles
setw -g window-status-style 'fg=colour9 bg=colour38'
setw -g window-status-format ' #I#[fg=colour56]:#[fg=colour55]#W#[fg=colour244]#F '

# Messages
set -g message-style 'fg=colour38 bg=colour192 bold'

# Allow local customizations in ~/.tmux_local.conf
if-shell "[ -f ~/.tmux_local.conf ]" 'source ~/.tmux_local.conf'

# MUST BE AT BOTTOM Plugin Manager
run -b '~/.tmux/plugins/tpm/tpm'
