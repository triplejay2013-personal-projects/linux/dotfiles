" Simplified RC for non-UNIX apps and environments (Obsidian in this case)

let mapleader = ","
let maplocalleader = "\,"
let python_highlight_all=1


" Setting up filetype indentation

au BufNewFile, BufRead *.py
            \ set tabstop=4 |
            \ set softtabstop=4 |
            \ set shiftwidth=4 |
            \ set textwidth=120 |
            \ set expandtab |
            \ set autoindent |
            \ set fileformat=unix
            \ set tabstop=2 |
            \ set softtabstop=2 |
            \ set shiftwidth=2

highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead, BufNewFile *.py, *.pyw, *.c, *.h match BadWhitespace /\s\+$/

" Toggle paste settings
set pastetoggle=<F3>

" toggle relative numbering
nnoremap <C-n> :set rnu!<CR>

" Enable folding with the spacebar
noremap <space> za

" Override ctl-w (switch window) with easier mapping
nnoremap <C-j> <C-w>j
nnoremap <C-K> <C-w>k
" nnoremap <C-l> <C-w>l " TODO conflicts with clear search results
nnoremap <C-h> <C-w>h

" Clear search results
nmap <silent> <C-L> <C-L>:nohlsearch<CR>:match<CR>:diffupdate<CR>

" unbind keys
map <C-a> <Nop>
map <C-x> <Nop>
nmap Q <Nop>

" Perform spell checking in current buffer only
command! Spell setlocal spell spelllang=en_us
command! NoSpell set nospell

set updatetime=200

set nocompatible  " Don't try to be vi compatible

" setting horizontal and vertical splits
set splitbelow
set splitright

" Enable folding
set foldmethod=manual
set foldlevel=99

set viminfo='100,<50,s10,h,f1  " Allow global marks, other persistent vim info
set tabpagemax=100  " Allow more tabs to be open

" TODO copy-pasta, might need some tweaking (remove message when done)
set ruler  " Show file stats
set shortmess+=I " disable startup message
set nu " number lines
set showmatch  " Show matching braces when text indicator is over them
set rnu " relative line numbering
" set incsearch " incremental search (as string is being typed)
set hls " highlight search
" set nohlsearch " disable highlights
set lbr " line break
set scrolloff=5 " show lines above and below cursor (when possible)
set noshowmode " hide mode
set laststatus=2
set modelines=0  " Security
set list
set listchars=tab:>-,trail:~
set backspace=indent,eol,start " allow backspacing over everything
set timeout timeoutlen=1000 ttimeoutlen=100 " fix slow O inserts
set lazyredraw " skip redrawing screen in some cases
set autochdir " automatically set current directory to directory of last opened file
set hidden " allow auto-hiding of edited buffers
set history=8192 " more history
set nojoinspaces " suppress inserting two spaces between sentences
" use 4 spaces instead of tabs during formatting
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
" smart case-sensitive search
set ignorecase
set smartcase
" tab completion for files/bufferss
set wildmode=longest,list
set wildmenu
set mouse+=a " enable mouse mode (scrolling, selection, etc)
if &term =~ '^screen'
    " tmux knows the extended mouse mode
    set ttymouse=xterm2
endif
set nofoldenable " disable folding by default
set wrap
set textwidth=120
set noshiftround
set formatoptions=tcqrn1
set colorcolumn=88,120

" disable audible bell
set noerrorbells visualbell t_vb=

" open new split panes to right and bottom, feels more natural
set splitbelow
set splitright

" fix lagging cursor
set ttyfast
set lazyredraw
" This setting slows down redraw (see help). If experiencing a ton of slowdown, then disable
set cul!
