# About

_Documentation [here](https://learnvimscriptthehardway.stevelosh.com/chapters/42.html)_

# Structure

* `ftdetect`: filetype detection. Autocommands that detect and set filetype of files
* `ftplugin`:
* `pack/vendor/start`: vim plugins
* `view`: ??


# https://vi.stackexchange.com/a/20067
Centralized location for swap files

* `swap`:
* `undo`:
* `backup`:
