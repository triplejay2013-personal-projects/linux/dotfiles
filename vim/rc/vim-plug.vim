" vim-plug setup
call plug#begin('~/.vim/plugged')
" Make sure to use single quotes

" See junegunn/vim-plug#readme

Plug 'chrisbra/NrrwRgn'
Plug 'ycm-core/YouCompleteMe', {'do': './install.py --clangd-completer'}
Plug 'jiangmiao/auto-pairs'
Plug 'jlanzarotta/bufexplorer'
" Disabling in favor of fzf
" Plug 'ctrlpvim/ctrlp.vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'sjl/gundo.vim'
Plug 'haya14busa/incsearch.vim'
Plug 'davidhalter/jedi-vim'
Plug 'preservim/nerdtree'
Plug 'mhartington/oceanic-next'
Plug 'ervandew/supertab'
" Plug 'vim-syntastic/syntastic' " Disabling in favor of ALE for now
" vim-markdown provides folding/syntax highlilghting for markdown
Plug 'godlygeek/tabular' | Plug 'plasticboy/vim-markdown'
" Live preview of markdown, technically a neovim plugin, but should work with vim 8.0
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }
" limelight helps focus on where the developer is currently typing
" goyo is a 'no distraction mode' setting for vim. Removes....distractions :)
Plug 'junegunn/goyo.vim' | Plug 'junegunn/limelight.vim'
Plug 'preservim/tagbar'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'FooSoft/vim-argwrap'
" NOTE: Electing to use bufexplorer instead
" Plug 'jeetsukumaran/vim-buffergator'
Plug 'altercation/vim-colors-solarized'
Plug 'tpope/vim-commentary'
Plug 'nvie/vim-flake8'
Plug 'tpope/vim-fugitive'
Plug 'mattn/vim-gist'
Plug 'tpope/vim-git'
Plug 'airblade/vim-gitgutter'
Plug 'pangloss/vim-javascript'
Plug 'yuezk/vim-js'
Plug 'tpope/vim-liquid'
Plug 'tpope/vim-repeat'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-scriptease'
Plug 'tpope/vim-sensible'
Plug 'kshenoy/vim-signature'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-obsession'
Plug 'hashivim/vim-terraform'
Plug 'tmux-plugins/vim-tmux'
Plug 'roxma/vim-tmux-clipboard'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'cespare/vim-toml'
Plug 'tshirtman/vim-cython'

" ===
" FZF
" ===
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" =========
" Ultisnips
" =========
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" =====
" React
" =====
Plug 'prettier/vim-prettier', { 'do': 'npm install' }
" ReactJS templating syntax helper
Plug 'maxmellon/vim-jsx-pretty'
" Typescript support
Plug 'HerringtonDarkholme/yats.vim'
" Close HTML tags
Plug 'alvan/vim-closetag'
" Asynchronous Lint Engine
Plug 'dense-analysis/ale'
" Javascript linting addon to Syntastic
Plug 'xojs/vim-xo'

" NOTE: vim-devicons must be the last plugin installed
Plug 'ryanoasis/vim-devicons'


" Initialize plugin System
call plug#end()

" Run PlugInstall if there are missing plugins
" This WILL increase the load time of vim
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
    \| PlugInstall --sync | source $MYVIMRC
\| endif
