" --------------------------
" Key remaps and Keybindings
" --------------------------

" Toggle paste settings
set pastetoggle=<F3>

" toggle relative numbering
nnoremap <C-n> :set rnu!<CR>

" Enable folding with the spacebar
noremap <space> za

" Override ctl-w (switch window) with easier mapping
nnoremap <C-j> <C-w>j
nnoremap <C-K> <C-w>k
" nnoremap <C-l> <C-w>l " TODO conflicts with clear search results
nnoremap <C-h> <C-w>h

" Clear search results
nmap <silent> <C-L> <C-L>:nohlsearch<CR>:match<CR>:diffupdate<CR>

" Plugin Bindings
" ALE Plug binds
nmap <leader>{ <Plug>(ale_first)
nmap <leader>[ <Plug>(ale_previous_wrap)
nmap <leader>] <Plug>(ale_next_wrap)
nmap <leader>} <Plug>(ale_last)
nmap <leader>, <Plug>(ale_fix)

" unbind keys
map <C-a> <Nop>
map <C-x> <Nop>
nmap Q <Nop>

" ---
" fzf
" ---
nnoremap <silent> <C-p> :Files<CR>

" -----------------
" Switching Buffers
" -----------------
nnoremap <leader><C-l> :bnext<CR>
nnoremap <leader><C-h> :bprev<CR>
