" Set highlight group
hi clear SpellBad
hi SpellBad cterm=underline
hi SpellBad ctermfg=DarkBlue
hi SpellBad ctermbg=DarkYellow
hi SpellLocal ctermfg=NONE
hi SpellLocal ctermbg=NONE
