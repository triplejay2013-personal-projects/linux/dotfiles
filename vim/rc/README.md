# Vim extra configurations

This provides logical separation of vimrc configurations.


# Structure

`autocmd.vim`: Automatic commands
`bindings.vim`: Vim custom keybindings (usually for plugins)
`commands.vim`: Custom vim commands and functions
`plugins.vim`: Configure downloaded plugins
`README.md`: THIS FILE
`settings.vim`: Vim-only settings
`spell.vim`: Spelling config
`vim-plug.vim`: List of plugins
