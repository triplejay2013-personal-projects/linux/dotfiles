" ------------
" AutoCmd defs
" ------------

" highlight current line, but only in active window
augroup CursorLineOnlyInActiveWindow
    autocmd!
    autocmd VimEnter,WinEnter,BufWinEnter * setlocal cursorline
    autocmd WinLeave * setlocal nocursorline
augroup END

" Persist Folds ref: https://til.hashrocket.com/posts/17c44eda91-persistent-folds-between-vim-sessions
augroup AutoSaveFolds
    " Fix by: https://vi.stackexchange.com/a/13874
  autocmd!
  " view files are about 500 bytes
  " bufleave but not bufwinleave captures closing 2nd tab
  " nested is needed by bufwrite* (if triggered via other autocmd)
  autocmd BufWinLeave,BufLeave,BufWritePost ?* nested silent! mkview!
  autocmd BufWinEnter ?* silent! loadview
augroup end

" Setting up filetype indentation

au BufNewFile, BufRead *.py
            \ set tabstop=4 |
            \ set softtabstop=4 |
            \ set shiftwidth=4 |
            \ set textwidth=120 |
            \ set expandtab |
            \ set autoindent |
            \ set fileformat=unix

au BufNewFile, BufRead *.js, *.html, *.css
            \ set tabstop=2 |
            \ set softtabstop=2 |
            \ set shiftwidth=2

highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead, BufNewFile *.py, *.pyw, *.c, *.h match BadWhitespace /\s\+$/

" Save sessions and close nerd tree
autocmd VimLeave * NERDTreeClose

" Automatically configure CWD
" autocmd BufEnter * silent! lcd %:p:h

" Automatically configure CWD
" Sets root to git root if possible
autocmd BufEnter * silent! Gcd
