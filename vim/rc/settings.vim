" NOTE: Before modifying this configfile, review vim-sensible to see if the
" setting is already taken care of.

set nocompatible  " Don't try to be vi compatible

" Relocate backup/swap/undo files
" NOTE: Use {path}// for full path: https://stackoverflow.com/a/21026618
" This 'swaps' the '/' with a '%' and provides FULL path to file (ensures unique)
set directory=~/.vim/swap//
set backupdir=~/.vim/backup//
set undodir=~/.vim/undo//

" setting horizontal and vertical splits
set splitbelow
set splitright

" Enable folding
set foldmethod=manual
set foldlevel=99

set viminfo='100,<50,s10,h,f1  " Allow global marks, other persistent vim info
set tabpagemax=100  " Allow more tabs to be open

" TODO copy-pasta, might need some tweaking (remove message when done)
set ruler  " Show file stats
set shortmess+=I " disable startup message
set nu " number lines
set showmatch  " Show matching braces when text indicator is over them
set rnu " relative line numbering
" set incsearch " incremental search (as string is being typed)
set hls " highlight search
" set nohlsearch " disable highlights
set lbr " line break
set scrolloff=5 " show lines above and below cursor (when possible)
set noshowmode " hide mode
set laststatus=2
set modelines=0  " Security
set list
set listchars=tab:>-,trail:~
set backspace=indent,eol,start " allow backspacing over everything
set timeout timeoutlen=1000 ttimeoutlen=100 " fix slow O inserts
set lazyredraw " skip redrawing screen in some cases
set hidden " allow auto-hiding of edited buffers
set history=8192 " more history
set nojoinspaces " suppress inserting two spaces between sentences
" use 4 spaces instead of tabs during formatting
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
" smart case-sensitive search
set ignorecase
set smartcase
" tab completion for files/bufferss
set wildmode=longest,list
set wildmenu
set mouse+=a " enable mouse mode (scrolling, selection, etc)
if &term =~ '^screen'
    " tmux knows the extended mouse mode
    set ttymouse=xterm2
endif
set nofoldenable " disable folding by default
set wrap
set textwidth=120
set noshiftround
set formatoptions=tcqrn1
set colorcolumn=88,120

" disable audible bell
set noerrorbells visualbell t_vb=

" open new split panes to right and bottom, feels more natural
set splitbelow
set splitright

" fix lagging cursor
set ttyfast
set lazyredraw
" This setting slows down redraw (see help). If experiencing a ton of slowdown, then disable
set cul!
" When restoring vim sessions, ignore blank files (nerd tree) to avoid errors
" https://github.com/preservim/nerdtree/issues/745#issuecomment-568509155
set sessionoptions-=blank
" NOTE: This setting causes issues in workflow (no known root)
" set autochdir " automatically set current directory to directory of last opened file"
