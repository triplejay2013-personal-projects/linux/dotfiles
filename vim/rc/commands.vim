"----------------
" Custom commands
"----------------

command! Reformat !python3 -m black -l 120 %
command! StyleCheck !python3 -m pydocstyle %
command! ImportCheck !python3 -m isort %

" Perform spell checking in current buffer only
command! Spell setlocal spell spelllang=en_us
command! NoSpell set nospell

"save read-only files
command! -nargs=0 Sudow w !sudo tee % >/dev/null

" Tmux
" Find pane with swp file if encountered: https://unix.stackexchange.com/a/309662
command! TmuxSwp !source ~/.shell/functions.sh && tmux-find-swp %:p

" TODO it would be nice to have a quick info button (F1 maybe?) to list all F#
" functionalities that I have mapped
"

function! RipgrepFzf(query, fullscreen)
  let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
  call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)
