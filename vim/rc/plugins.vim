" ----------------------------------------------
" Configuration settings for third-party plugins
" ----------------------------------------------

" -------------
" vim-gitgutter
" -------------
set updatetime=200

" ---------
" Syntastic
" ---------
" let g:syntastic_python_checkers=['flake8 --max-line-length 88']

" -----------
" vim-airline
" -----------
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='oceanicnext'

" --------
" jedi-vim
" --------
let g:jedi#completions_enabled = 0 " default to YCMs autocomplete (which uses jedi)
let g:jedi#force_py_version = '3'
" Automatically search venv if exists
" TODO bugged?
" let g:jedi#environment_path = "venv"

" ------------
" oceanic-next
" ------------
colorscheme OceanicNext

" -----
" ctrlp
" -----
" set runtimepath^=~/.vim/bundle/ctrlp.vim

" ------
" tagbar
" ------
nmap <F8> :TagbarToggle<CR>

" --------
" NERDTree
" --------
" autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
let NERDTreeWinSize = 50
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") && v:this_session == "" | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
nnoremap <Leader>N :NERDTreeToggle<CR>
nnoremap <Leader>f :NERDTreeFind<CR>
" autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
let g:NerdTreeChDirMode=2

" ---
" YCM
" ---
set encoding=utf-8
let g:ycm_python_interpreter_path = ''
let g:ycm_python_sys_path = []
let g:ycm_extra_conf_vim_data = [
  \  'g:ycm_python_interpreter_path',
  \  'g:ycm_python_sys_path'
  \]
let g:ycm_global_ycm_extra_conf = '~/.global_extra_conf.py'
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR>


" --------
" CloseTag
" --------
let g:closetag_filenames = '*.html,*.xhtml,*.xml,*.js,*.jsx,*.tsx,*.html.erb,*.md'
let g:closetag_xhtml_filenames = '*.js,*.jsx,*.tsx'

" ---
" ALE
" ---
let b:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'python': ['isort', 'black'],
\   'javascript': ['eslint'],
\   'html': ['prettier'],
\   'typescript': ['prettier', 'tslint'],
\   'scss': ['prettier'],
\   'reason': ['refmt'],
\}

let b:ale_linters = {
\   'python': ['pyflakes'],
\   'javascript': ['eslint'],
\}

let g:ale_javascript_xo_options = "--plug=react --prettier"
let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 0  " I use YCM
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'
let g:airline#extensions#ale#enabled = 1  " Makes compatible with vim-airline
let g:ale_pattern_options = {'\.json$': {'ale_enabled': 0}}
let g:ale_python_flake8_options = '--max-line-length=120'
" I prefer ll of 120, but this isn't compatible with some projects
" let g:ale_python_black_options = '--line-length=120'

" --------
" Prettier
" --------
"  TODO These don't belong here, but leaving here while testing things out
au FileType javascript setlocal formatprg=prettier
au FileType javascript.jsx setlocal formatprg=prettier
au FileType typescript setlocal formatprg=prettier\ - parser\ typescript


" --------------------------------------
" Ultisnips / Supertab / YCM integration
" Refs:
"   - https://gist.github.com/lencioni/dff45cd3d1f0e5e23fe6 (gist)
"   - https://stackoverflow.com/a/22253548 (SO answer)
"   - https://github.com/SirVer/ultisnips
"   - https://github.com/ervandew/supertab
"   - https://github.com/ycm-core/YouCompleteMe
" Notes:
"   The links above are interesting, but outdated. YCM/SuperTab/UltiSnips all integrate
"   with eachother. YCM Defaults to ultisnips if it is installed, and ultisnips will
"   default to supertab if installed. All I need (as per docs) is to define my own
"   expand trigger. If tabs break, it is likely the fault of UltiSnips, YCM, or SuperTab
" --------------------------------------
let g:UltiSnipsExpandTrigger           = '<C-l>'

" Let UltiSnips discover my personal snippets
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my-snippets"]

" This is a vim-snippets flag
" Use google-styled docstrings
let g:ultisnips_python_style="google"

" .....................................................................................
" junegunn/limelight.vim options (with Goyo integration)
" .....................................................................................

" Normal and visual mode mappings
" nmap <Leader>l <Plug>(Limelight)
" xmap <Leader>l <Plug>(Limelight)

" Do not overrule hlsearch
let g:limelight_priority = -1

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!


" .....................................................................................
" plasticboy/vim-markdown
" .....................................................................................
let g:vim_markdown_frontmatter=1
let g:mkdp_port=8181

autocmd FileType markdown let b:sleuth_automatic=0
autocmd FileType markdown set conceallevel=0
autocmd FileType markdown normal zR


" .....................................................................................
" iamcco/markdown-preview.nvim
" .....................................................................................
let g:mkdp_refresh_slow=1
let g:mkdp_markdown_css='/home/justin/.local/lib/github-markdown-css/github-markdown.css'
