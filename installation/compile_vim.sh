#!/bin/bash

# Compile vim configuration into a single rc file (useful for environments where
# my segmented vim setup won't work)

pushd $HOME/.dotfiles

COMPILED="compiled_vim"
VIMPLUG="vim/rc/vim-plug.vim"

touch $COMPILED
cat $VIMPLUG > $COMPILED
GLOBAL_SETTINGS=$(cat <<-EOF
let mapleader = ","\n
let maplocalleader = "\\,"\n
let python_highlight_all=1\n
EOF
)
echo -e $GLOBAL_SETTINGS >> $COMPILED

for f in vim/rc/*.vim; do
    if [[ $f != $VIMPLUG ]]; then
        echo Copying $f into $COMPILED
        cat $f >> $COMPILED
    fi
done
popd >> /dev/null

if [[ -d $HOME/.dotfiles-local ]]; then
    pushd $HOME/.dotfiles-local
    for f in vim/rc-local/*.vim; do
        echo Copying $f into $COMPILED
        cat $f >> $COMPILED
    done
    popd >> /dev/null
fi
