# Structure

* `dependencies.sh`: Checks for installed packages in current environment
* `install.conf.yaml`: dotbot installation config
* `post-install`: dotbot post-installation config
* `update.conf.yaml`: dotbot update config


# About

The idea with dotbot, is to provide a utility to setup symlinks, and install packages
from a single utility (as opposed to a script that might rely on multiple, dotbot has
knowledge of these things innately).

The install config will install most basic things, the post-install handles anything
that for whatever reason cannot be installed until after a reboot, or initial install.
The update is intended to be idempotent, and rerun after installation solely. The install
scripts should only be run once, while the update is intended to keep packages up to date, but
not download existing ones, or do lengthy installation processes each time.

