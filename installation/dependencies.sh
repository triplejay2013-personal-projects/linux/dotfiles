#!/usr/bin/env bash

# Assert Certain dependencies exist

PY_VER=$(python3 --version)
if ! [[ $PY_VER =~ "Python 3.8" ]]; then
    echo "Expecting python3.8.4 to be in environment, instead got $PY_VER"
    exit 1
else
    echo "$PY_VER found (installed)"
fi

## PIP
pip3 --version >> /dev/null
if ! [[ $? -eq 0 ]]; then
    echo "Must install pip3 to continue with installation script"
    exit 1
else
    echo "pip3 found (installed)"
    PIP_REQUIRE_VIRTUALENV=false python3 -m pip install --upgrade pip
fi

## COMMON# Install our common module as a dependency (used by dotbot-deps)
if ! [ -d "$HOME/.common" ]; then
    echo "Cloning common repository"
    git clone --quiet git@gitlab.com:triplejay2013-personal-projects/utility/common.git $HOME/.common
else
    echo "Updating common repository"
    pushd $HOME/.common > /dev/null; git pull --ff-only --quiet; popd > /dev/null
fi
if ! [ $? -eq 0 ]; then
    echo "Common module doesn't exist or unable to be retrieved"
    exit $?
fi
echo "Loading shell functions"
source $HOME/.dotfiles/shell/functions.sh  # Load syspip3 manually
# Use grep because rg isn't installed yet
VERSION=`cat $HOME/.dotfiles/config/pip-requirements.txt | grep common/python/dist/common- | xargs basename`
echo "Installing common modules"
PIP_REQUIRE_VIRTUALENV=false python3 -m pip install --upgrade --force-reinstall $HOME/.common/python/dist/$VERSION > /dev/null

## DOTBOT
if ! [ -d "$HOME/.dotbot" ]; then
    echo "Cloning dotbot repository"
    git clone --quiet git@github.com:anishathalye/dotbot.git $HOME/.dotbot
else
    echo "Updating dotbot repository"
    pushd $HOME/.dotbot > /dev/null; git pull --ff-only --quiet; popd > /dev/null
fi
if ! [ $? -eq 0 ]; then
    echo "dotbot module doesn't exist or unable to be retrieved"
    exit $?
fi

## DOTBOT PLUGINS
if ! [ -d "$HOME/.dotbot-deps" ]; then
    echo "Cloning dotbot-deps repository"
    git clone --quiet git@gitlab.com:triplejay2013-personal-projects/utility/dotbot-deps.git $HOME/.dotbot-deps
else
    echo "Updating dotbot-deps repository"
    pushd $HOME/.dotbot-deps > /dev/null; git pull --ff-only --quiet; popd > /dev/null
fi
if ! [ $? -eq 0 ]; then
    echo "dotbot-deps module doesn't exist or unable to be retrieved"
    exit $?
fi
