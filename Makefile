# Control installation of things

.PHONY: installation post_install update dependencies reminders help
SHELL := /bin/bash

# Make sure to include this before any 'include' directives (if any)
THIS_FILE := $(lastword $(MAKEFILE_LIST))
default: help

DOTBOT_DIR := $$HOME/.dotbot
DOTBOT_DEPS_DIR := $$HOME/.dotbot-deps

# Overrides
BASEDIR ?= $$HOME/.dotfiles
INSTALL_DIR ?= $$HOME/.dotfiles/installation

# MyPlugins
_PIP := ${DOTBOT_DEPS_DIR}/pip.py
_WEB := ${DOTBOT_DEPS_DIR}/web.py
_GO := ${DOTBOT_DEPS_DIR}/go.py

INSTALL_CONFIG := ${INSTALL_DIR}/install.conf.yaml
POST_INSTALL_CONFIG := ${INSTALL_DIR}/post-install.conf.yaml
UPDATE_CONFIG := ${INSTALL_DIR}/update.conf.yaml

DOTBOT_BIN := bin/dotbot
DIRECTIVE := ${DOTBOT_DIR}/${DOTBOT_BIN} -d ${BASEDIR}
PLUGINS := --plugin ${_PIP} --plugin ${_WEB} --plugin ${_GO}

PYTHON3_VERSION ?= 3.8.10
FORCE ?= false
PREFIX ?=

install: dependencies  ## Install dotfiles directives
	@echo "Installing from config: ${INSTALL_CONFIG}"
	@${DIRECTIVE} -c ${INSTALL_CONFIG} ${PLUGINS}
	@echo "\n"
	@$(MAKE) -f $(THIS_FILE) reminders

post_install: dependencies  ## Post-Install dotfiles directives
	@echo "Installing from config: ${POST_INSTALL_CONFIG}"
	@${DIRECTIVE} -c ${POST_INSTALL_CONFIG} ${PLUGINS}

update: dependencies  ## Update dotfiles directives
	@echo "Updating from config: ${UPDATE_CONFIG}"
	@${DIRECTIVE} -c ${UPDATE_CONFIG} ${PLUGINS}

pip:
	@echo NOT IMPLEMENTED

# https://www.build-python-from-source.com/
python3:
	@./installation/install-python3.sh $(PYTHON3_VERSION) $(FORCE) $(PREFIX)

dependencies: pip python3  ## Make sure dependencies exist
	@./installation/dependencies.sh

reminders:  ## Post-installation reminders
	@echo "Make sure to exit shell or manually load bashrc to see changes affect current session."
	@echo "then -> Make sure to also run 'vim' at least once to initiate install script (will take a minute or two)"


compile_vim:  ## Compile vim config into single rc file
	@./installation/compile_vim.sh

# https://nedbatchelder.com/blog/201804/makefile_help_target.html
help:  ## Displays options for make
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
