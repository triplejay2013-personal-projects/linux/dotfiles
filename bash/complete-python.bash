# Split out Python completions

# This isn't necessary. This is done for readability, since these can add up
# TODO: These need to be more robust, since it isn't always clear when one of them breaks.
# This can be done with logfiles, try/catch (bash equiv.) or some other method

# python Click completion
# https://click.palletsprojects.com/en/8.0.x/shell-completion/?highlight=shell_complete
eval "$(_MYSETTINGS_COMPLETE=bash_source mysettings)"
eval "$(_MYSIMPLESECRETS_COMPLETE=bash_source mysimplesecrets)"

#Auto-Complete function for AWSume
_awsume() {
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=$(awsume-autocomplete)
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0
}
complete -F _awsume awsume
