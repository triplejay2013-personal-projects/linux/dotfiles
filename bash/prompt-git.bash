# All globals/methods should be prefixed with __PROMPT in this file

check_and_source $HOME/.bash/colors.bash __COLORS_LOADED

# 1 = enabled, 0 = disabled
GIT_EXTRAS_ENABLED=1
PYTHON_ASYNC_PROMPT=`command -v mybashprompt &> /dev/null && echo 1 || echo 0`

__git_branch() {
    local git_branch=$1
    if [[ -z $git_branch ]]; then
        git_branch=$(echo $(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'))
    fi
    if [[ -z $git_branch ]]; then
        echo "null"
    elif [[ $git_branch == "master" ]]; then
        echo "master"
    elif [[ $git_branch == "main" ]]; then
        echo "main"
    else
        echo $git_branch
    fi
}

__prompt_git_up_to_date() {
    if [[ $PYTHON_ASYNC_PROMPT -eq 1 ]]; then
        local needs_update=$1
        local extra=${__GIT_UP_TO_DATE_EXTRA:-}
        local response="$(ps_ CYN) *update$(ps_ RS)$extra"
        if [[ $needs_update == "True" ]]; then
            echo "$response"
        fi
    fi
}

__prompt_git_commit_count() {
    if [[ $PYTHON_ASYNC_PROMPT -eq 1 ]]; then
        local commit_count=$1
        local extra=${__GIT_COMMIT_COUNT_EXTRA:-}
        echo "$(ps_ YEL)[log $(ps_ RS HC BLE)$commit_count$(ps_ RS YEL)]$(ps_ RS)$extra"
    fi
}

__prompt_git_branch_count() {
    if [[ $PYTHON_ASYNC_PROMPT -eq 1 ]]; then
        local extra=${__GIT_BRANCH_LOCAL_COUNT_EXTRA:-}
        if [[ -z $1 ]] && [[ -z $2 ]] || [[ $(__git_branch) != "null" ]]; then
            local lcount=$1
            if [[ -z $lcount ]]; then
                lcount=$(git branch 2>&1 | wc -l)
            fi
            local lcolor=$(ps_ RS HC CYN)  # l - local
            if [[ $lcount -eq 1 ]]; then
                lcolor=$(ps_ RS HC MAG)
            fi
            local rcount=$2
            if [[ -z $rcount ]]; then
                rcount=$(git branch -r 2>&1 | wc -l)
            fi
            local rcolor=$(ps_ RS HC CYN)  # r - remote
            if [[ $rcount -eq 1 ]]; then
                rcolor=$(ps_ RS HC MAG)
            fi
            if [[ $lcount -eq 0 ]] || [[ $rcount -eq 0 ]]; then
                return
            fi
            echo "$(ps_ RS YEL)[local $lcolor$lcount$(ps_ RS YEL)][remote $rcolor$rcount$(ps_ RS YEL)]$(ps_ RS)$extra"
        fi
    fi
}

__prompt_git_branch() {
    local git_branch=$1
    if [[ -z $git_branch ]]; then
        git_branch=$(__git_branch)
    fi

    local out
    case $git_branch in
        null) echo $extra; return;;
        master) out="$(ps_ RS HC RED) ${git_branch}";;
        main) out="$(ps_ RS HC RED) ${git_branch}";;
        *) out="$(ps_ RS HC YEL) ${git_branch}";;
    esac

    local git_root=$2
    if [[ -z $git_root ]]; then
        git_root=$(git root 2>&1)
    fi
    local extra=${__GIT_BRANCH_EXTRA:-}
    local git_root_extra=${__GIT_BRANCH_GIT_ROOT_EXTRA:-}
    if [[ -z $git_root ]]; then
        # Don't report anything if we aren't in a git repo
        echo
    else
        echo "\n$(ps_ YEL)[$(ps_ UL GRN)$git_root$out$git_root_extra$(ps_ RS YEL)]$(ps_ RS)$extra"
    fi
}

# Flag to show this file has been sourced in current runtime
__PROMPT_GIT_LOADED=true
