# Partially Adapted from: https://stackoverflow.com/a/36876883

# Quote concatenated variables with begin/end
# # Example
# # A=aaa; B=bbb; C=ccc
# # qut '[' ']' A B C
# # ==> [aaabbbccc]
qut(){
    local end_; end_=$2
    local code; code=$1
    shift 2
    while (( $# > 0 )); do
        code+=${!1}
        shift 1
    done
    code+="$end_"
    echo $code
}

# Escape non printable codes for shell prompting
ps_(){
    qut "\[" "\]" "$@"
}

if [[ -n "$TERM" ]]; then
    # ANSI color codes
    RS=$(tput sgr0)         # reset
    NC=$(tput sgr0)         # reset (No Color)
    HC=$(tput bold)         # hicolor
    UL=$(tput smul)         # underline
    INV=$(tput rev)         # inverse background and foreground
    BLK=$(tput setaf 0)     # Black
    RED=$(tput setaf 1)     # Red
    GRN=$(tput setaf 2)     # Green
    YEL=$(tput setaf 3)     # Yellow
    BLE=$(tput setaf 4)     # Blue
    MAG=$(tput setaf 5)     # Magenta/Purple
    CYN=$(tput setaf 6)     # Cyan
    WHT=$(tput setaf 7)     # White

    # Flag to show this file has been sourced in current runtime
    __COLORS_LOADED=true
fi
