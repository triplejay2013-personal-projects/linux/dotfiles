# Structure

Most of these are self-explanatory, but here we are anyways

* `colors.bash`: Splits out 'prompt.bash' to allow the color settings to be imported separately
* `complete.bash`: Completion functions (NOTE: These are notorious for breaking login shells in my experience)
* `complete-python.bash`: Splits out Python Click completions (because reasons...)
* `environment.bash`: Common environment settings (customized values belong in dotfiles-local)
* `plugins.bash`: Plugins
* `prompt.bash`: PS# configuration
* `prompt.git`: Breakout of git customizations (for readability)
* `README.md`: THIS FILE
* `settings.bash`: Bash settings
