# Needs to be updated if these are not the default loc.
export DOTFILES=~/.dotfiles
export DOTLOCAL=~/.dotfiles-local
export SWAP=~/.dotfiles/vim/swap
export UNDO=~/.dotfiles/vim/undo
export BACKUP=~/.dotfiles/vim/backup
export GOPATH=$HOME/go
