# All globals/methods should be prefixed with __PROMPT in this file

# Look up keycodes in man 5 terminfo
# Check keycodes for the terminal with the bash "read" command

check_and_source $HOME/.bash/colors.bash __COLORS_LOADED
check_and_source $HOME/.bash/prompt-git.bash __PROMPT_GIT_LOADED
check_and_source $HOME/.bash-local/prompt.bash __LOCAL_PROMPT_LOADED true

__prompt_machine_name() {
    local machine_name=$1
    local extra=${__MACHINE_NAME_EXTRA:-}
    if [[ -z $machine_name ]]; then
        machine_name=$(hostname)
    fi
    if [[ -f $HOME/.name ]]; then machine_name=$(cat $HOME/.name); fi
    echo "$(ps_ MAG)$machine_name$extra$(ps_ RS)"
}

__prompt_in_venv(){
    local extra=${__PROMPT_IN_VENV_EXTRA:-}
    if [[ -n $VIRTUAL_ENV ]]; then
        echo " $(ps_ BLE)(venv)$(ps_ RS)$extra"
    fi
}

__prompt_user_info(){
    local extra=${__USER_INFO_EXTRA:-}
    echo "$(ps_ BLE)#$(ps_ RS) $(ps_ CYN)\\u$(ps_ RS) $(ps_ GRN)at $(__prompt_machine_name) $(ps_ GRN)in $(ps_ RS YEL)\w$(ps_ RS)$extra"
}

__prompt() {
    local extra=${__PROMPT_EXTRA:-}
    echo "$(ps_ BLE)>$(ps_ RS)$extra "
}

bash_ps1_prompt() {
    local extra=${__PS1_PROMPT:-}
    if [[ $PYTHON_ASYNC_PROMPT -eq 1 ]]; then
        PS1="${__PS1_GIT_BRANCH}$__PS1_VENV\n${__PS1_USER_INFO}\n${__PS1_MY_PROMPT}"
    else
        PS1="\n${__PS1_USER_INFO}$__PS1_VENV\n${__PS1_MY_PROMPT}"
    fi
}

unset_mybashprompt_vars() {
    for v in ${!__GIT*}; do
        unset $v;
    done
}

bash_prompt_command() {
    # Executed every time the prompt shows up
    # https://tldp.org/HOWTO/Bash-Prompt-HOWTO/x264.html

    # PROMPT_COMMAND
    # https://bash.cyberciti.biz/guide/Changing_bash_prompt#PROMPT_COMMAND_variable

    unset_mybashprompt_vars
    if [[ $PYTHON_ASYNC_PROMPT -eq 1 ]]; then
        # Don't show git info if we don't have our async script
        if [[ $GIT_EXTRAS_ENABLED -eq 1 ]] && ! [[ -n $VIRTUAL_ENV ]]; then
            eval "$(mybashprompt bash_prompt)"
            local __GIT_BRANCH_LOCAL_COUNT_EXTRA=$(__prompt_git_commit_count $__GIT_COMMIT_COUNT)
            local __GIT_BRANCH_EXTRA=$(__prompt_git_branch_count $__GIT_LOCAL_BRANCH_COUNT $__GIT_REMOTE_BRANCH_COUNT)
            local __GIT_BRANCH_GIT_ROOT_EXTRA=$(__prompt_git_up_to_date $__GIT_BRANCH_NEEDS_UPDATE)
            # This refreshes dynamic components of our prompt
        fi
    fi
    __PS1_GIT_BRANCH=$(__prompt_git_branch $__GIT_BRANCH $__GIT_ROOT)

    __PS1_MACHINE_NAME=$(__prompt_machine_name $__MACHINE_NAME)
    __PS1_MY_PROMPT=$(__prompt)
    __PS1_USER_INFO=$(__prompt_user_info)
    __PS1_VENV=$(__prompt_in_venv)
    wait | true
    bash_ps1_prompt
}

PROMPT_DIRTRIM=3  # Shortens long paths to just n-number of names
# PROMPT_COMMAND=  # Executed just before displaying PS1
# PS1 - Default Interaction Prompt
# PS2 - Continuation Interactive Prompt (such as after a \ on a line)
# PS3 - Prompt used by 'select' inside a shell script
# PS4 - Used by 'set -x' to prefix tracing output

PROMPT_COMMAND=bash_prompt_command
# https://askubuntu.com/a/339925
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

PS2="$(__prompt) "

# Flag to show this file has been sourced in current runtime
__PROMPT_LOADED=true
