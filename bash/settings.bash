HISTSIZE=1048576
HISTFILE="$HOME/.bash_history"
HISTCONTROL=ignoredups:erasedups
SAVEHIST=$HISTSIZE
shopt -s histappend  # append to history file

export EDITOR=vim
