" Load vim-plug plugins
let $VIMPLUG=expand("~/.vim/rc/vim-plug.vim")
source $VIMPLUG

" Leader setup
let mapleader = ","
let maplocalleader = "\,"

" Setting up pyflakes
let python_highlight_all=1

" Dynamically load extra configuration files
" https://vimhelp.org/eval.txt.html#globpath%28%29
for f in globpath('~/.vim/rc/', '*.vim', 0, 1)
    let $LOCALFILE=expand(f)
    if $LOCALFILE != $VIMPLUG && filereadable($LOCALFILE)
        source $LOCALFILE
    endif
endfor

" Dynamically load extra configuration files (dotfiles-local)
for f in globpath('~/.vim/rc-local/', '*.vim', 0, 1)
    let $LOCALFILE=expand(f)
    if $LOCALFILE != $VIMPLUG && filereadable($LOCALFILE)
        source $LOCALFILE
    endif
endfor
