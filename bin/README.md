# About

Custom scripts

# Structure

* `mybashprompt`: Bash prompt customizations provided through async click application
* `mysettings`: An attempt to update Ubuntu gsettings and dconf options for profiles (buggy at best :/)
* `mysimplesecrets`: Python application to encrypt/decrypt files based off of private key (saved for me in LastPass)
* `README.md`: THIS FILE
* `tmx`: Launch tmux. Handles SSH mode as well, to attach to running session via ssh.
