<!-- Template from here: https://github.com/othneildrew/Best-README-Template/blob/master/README.md -->

<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->


<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url] -->
<!-- [![Forks][forks-shield]][forks-url] -->
<!-- [![Stargazers][stars-shield]][stars-url] -->
<!-- [![Issues][issues-shield]][issues-url] -->
[![MIT License][license-shield]][license-url]
<!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->


<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">dotfiles</h3>

  <p align="center">
    Personal (generic) configuration for Ubuntu devices
    <br />
    <a href="https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/blob/master/README.md"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    ·
    <a href="https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
        <li><a href="#branch-hierarchy">Branch Hierarchy</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

I hate reconfiguring multiple devices to contain my preferences. Or setting up a new
device and trying to remember how to do things properly. I stumbled upon `dotfiles`
while researching topics on a separate personal project that in essence was what was
implemented by `dotbot` (See acknowledgements). I scrapped that project and tried out
this implementation instead.


This project is derivative of `dotfiles`. It follows a similar structure, uses the same
tools, and mimics some of the setup as well. This project, however, contains my own
personal preferences and setup for an Ubuntu-like device (at time of writing).


For suggestions on how to do a similar setup, I would recommend following any of the
[guides](https://github.com/anishathalye/dotbot/wiki/Tutorials) that have already been
provided by others with a similar interest in a one-stop configuration utility. Any
issues/forks/pull requests ideally should come from the `dotfiles` repo and not my own.


This repository is meant to be a purely generic configuration for devices I commonly
use (that are Ubuntu based). For custom (and more personal) configurations, I am
following the `dotfiles-local` template. This setup allows overrides in
a variety of locations from a `.\<dotfile\>\_local` configuration file. My own
`dotfiles-local` repo is for private use and will not be available publicly :/


### Branch Hierarchy
--------------------
```
master: basic files/config that should work on any distribution (irrespective of flavor)
|
+-  ubuntu: Ubuntu extra configurations. These include programs installed via apt, and
|   |       may work on compatiable distributions (not guarenteed)
|   |
|   |  The following configs are examples of how I can tune the base branch for different releases
|   +- ubuntu-18: (example - doesn't exist)
|   |
|   +- ubuntu-20: (example - doesn't exist)
|   |
|   +- ubuntu-21: (example - doesn't exist).
```


### Built With

*NOTICE:* Make sure to update this list if anything is added. The list here are added
via `git submodule add` and are tracked by git in that fashion. See `Usage` link for
more details on why that is the case.

Legend:
- undefined: I haven't used the tool enough to really describe what it is in my own
  words. Sometimes this is because I saw someone else use something and wanted to try it
  out, but never got around to doing so.

  Something may be undefined even if I know what it does. I'll mark it as such until
  I have used the tool enough to understand it's purpose.

- .\*: Short description of what I think the tool is/does

* [dotbot-deps](https://gitlab.com/triplejay2013-personal-projects/utility/dotbot-deps) :: My own customized versions of apt & pip Plugins for dotbot
* [common](https://gitlab.com/triplejay2013-personal-projects/utility/common) :: My own common packages
* [jupyter-vim-binding](https://github.com/lambdalisue/jupyter-vim-binding) :: undefined
* [dircolors-solarized](https://github.com/seebi/dircolors-solarized) :: undefined
* [vim-plugins](https://www.swapreference.com/vim-vim-plugins/)
    * [NrrwRgn](https://github.com/chrisbra/NrrwRgn) :: undefined
    * [auto-pairs](https://github.com/jiangmiao/auto-pairs) :: undefined
    * [bufexplorer](https://github.com/jlanzarotta/bufexplorer) :: explore vim buffers with simple commands
    * [ctrlp.vim](https://github.com/ctrlpvim/ctrlp.vim) :: fuzzy finder
    * [editorconfig](https://github.com/editorconfig/editorconfig-vim) :: undefined
    * [gundo](https://github.com/sjl/gundo.vim) :: undefined
    * [incsearch](https://github.com/haya14busa/incsearch.vim) :: undefined
    * [jedi-vim](https://github.com/davidhalter/jedi-vim) :: syntax engine for python
    * [nerdtree](https://github.com/preservim/nerdtree) :: directory navigation window
    * [oceanic-next](https://github.com/mhartington/oceanic-next) :: soft color theme
    * [supertab](https://github.com/ervandew/supertab) :: undefined
    * [syntastic](https://github.com/vim-syntastic/syntastic) :: syntax engine
    * [tabular](https://github.com/godlygeek/tabular) :: Alignment tool
    * [tagbar](https://github.com/preservim/tagbar) :: Class specification viewer
    * [vim-airline](https://github.com/vim-airline/vim-airline) :: undefined
    * [vim-ailrline-themes](https://github.com/vim-airline/vim-airline-themes) :: undefined
    * [vim-argwrap](https://github.com/FooSoft/vim-argwrap) :: undefined
    * [vim-buffergator](https://github.com/jeetsukumaran/vim-buffergator) :: undefined
    * [vim-colors-solarized](https://github.com/altercation/vim-colors-solarized) :: undefined
    * [vim-commentary](https://github.com/tpope/vim-commentary) :: smart-commenting features
    * [vim-flake8](https://github.com/nvie/vim-flake8) :: python linter
    * [vim-fugitive](https://github.com/tpope/vim-fugitive) :: Git commands in vim
    * [vim-gist](https://github.com/mattn/vim-gist) :: undefined
    * [vim-git](https://github.com/tpope/vim-git) :: undefined
    * [vim-gitgutter](https://github.com/airblade/vim-gitgutter) :: show git line changes in gutter with symbols
    * [vim-javascript](https://github.com/pangloss/vim-javascript) :: undefined
    * [vim-js](https://github.com/yuezk/vim-js) :: undefined
    * [vim-liquid](https://github.com/tpope/vim-liquid) :: undefined
    * [vim-repeat](https://github.com/tpope/vim-repeat) :: '.' for plugins that support vim-repeat
    * [vim-ripgrep](https://github.com/jremmen/vim-ripgrep) :: faster grep in vim
    * [vim-scriptease](https://github.com/tpope/vim-scriptease) :: undefined
    * [vim-sensible](https://github.com/tpope/vim-sensible) :: base configuration of vimrc
    * [vim-signature](https://github.com/kshenoy/vim-signature) :: undefined
    * [vim-speeddating](https://github.com/tpope/vim-speeddating) :: modify dates with \<C-A\> etc.
    * [vim-surround](https://github.com/tpope/vim-surround) :: Modify things that surround text (like parens, braces, quotes etc.)
    * [vim-terraform](https://github.com/hashivim/vim-terraform) :: undefined
    * [vim-tmux](https://github.com/tmux-plugins/vim-tmux) :: undefined
    * [vim-tmux-clipboard](https://github.com/roxma/vim-tmux-clipboard) :: undefined
    * [vim-tmux-focus-events](https://github.com/tmux-plugins/vim-tmux-focus-events) :: undefined
    * [vim-toml](https://github.com/cespare/vim-toml) :: undefined



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

As per the original author's description, this is meant to be a standalone installation
process that doesn't require anything else other than a typical Unix distribution (in
this case expecting Ubuntu 20.\*)


### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles.git ~/.dotfiles
   ```

   _You don't HAVE to specify the installation name (the install will link to wherever)_

   ```sh
   git clone https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles.git
   ```

2. Install configuration script
   ```sh
   cd DOTFILES_DIR && ./install
   ```



<!-- USAGE EXAMPLES -->
## Usage

Use this space to add any aliases/commands/functions that I want to use, or use
regularly that would be useful to know. This can act as my own personal `cheatsheet` for
using my setup.

TODO add table

_For more examples, please refer to the [Documentation](https://github.com/anishathalye/dotbot/wiki/Tutorials)_



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

This project is intended for personal use. I don't expect collaboration but have
included a CHANGELOG, CONTRIBUTING etc just for completeness in my gitlab setup (it
serves as a template for other repos when I want to set one up). I don't actually expect
people to contribute to this project (and would prefer if you didn't :)). Feel free to
use any part of my config in your own setups as you see fit.


If any submodules are added, they should be included in the `Built With` section above.
I don't intend to update the CHANGELOG or even provide tags/release versions for this
project.



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Justin Johnson - triplejay2013@gmail.com

Project Link: [https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles](https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements

* [othneildrew - README Template](https://github.com/othneildrew/Best-README-Template)
* [anishathalye - dotfiles](https://github.com/anishathalye/dotfiles)
* [anishathalye - dotfiles-local](https://github.com/anishathalye/dotfiles-local)
* [anishathalye - dotbot](https://github.com/anishathalye/dotbot)




<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [contributors-shield]: https://img.shields.io/github/contributors/github_username/repo.svg?style=for-the-badge -->
<!-- [contributors-url]: https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/graphs/master -->
<!-- [forks-shield]: https://img.shields.io/github/forks/github_username/repo.svg?style=for-the-badge -->
<!-- [forks-url]: https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/-/forks/new -->
<!-- [stars-shield]: https://img.shields.io/github/stars/github_username/repo.svg?style=for-the-badge -->
<!-- [stars-url]: https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/starrers -->
<!-- [issues-shield]: https://img.shields.io/github/issues/github_username/repo.svg?style=for-the-badge -->
<!-- [issues-url]: https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/issues -->
[license-shield]: https://img.shields.io/badge/license-MIT-yellowgreen
[license-url]: https://gitlab.com/triplejay2013-personal-projects/linux/dotfiles/blob/master/LICENSE
<!-- [linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555 -->
<!-- [linkedin-url]: https://linkedin.com/in/triplejay2013 -->
