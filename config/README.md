# About

Package requirements for a variety of utilities

# Structure
* `packages.conf`: apt packages
* `pip-requirements.txt`: pip requirements
* `README.md`: THIS FILE
* `web-packages.yaml`: curl/wget scripts and files
